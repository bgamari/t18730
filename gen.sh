#!/usr/bin/env bash

set -e

n=$1

cat <<EOF
{-# LANGUAGE TupleSections #-}

module Test.Bonanza.Arbitrary where

import Gen (Gen)

genFields :: Gen [(String, Int)]
genFields =
  mapM
    (\(f, g) -> (f,) <$> g)
    [ ("field", genIntField)
EOF

for i in $(seq $n); do
  echo "    , (\"field_$n\", genIntField)"
done

cat <<EOF
    ]

genIntField :: Gen Int
genIntField = pure 0
EOF
