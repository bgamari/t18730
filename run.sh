#!/usr/bin/env bash

set -e


# Requires `random` library.

GHC=/opt/exp/ghc/ghc-landing/_build/stage1/bin/ghc
#GHC=ghc

run() {
  n="$1"; shift
  args="$@"
  echo "$i"
  bash ./gen.sh "$n" > Main.hs
  mkdir -p out-$n
  rm -f Main.{o,hi} Gen.{o,hi}
  /usr/bin/env time -o "out-$n/time" \
    $GHC \
    -O2 Main.hs -v2 -fforce-recomp \
    -dumpdir "out-$n" \
    $args \
    >& out-$n/log || echo "$n: failed $?"
  cat out-$n/log | grep 'Inlining done' | sort | uniq -c | sort -n | grep genFields || true
}

for i in $(seq 10); do
  run "$i" $@
done
